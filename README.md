# Simple local Kubernetes cluster
This repository provides a simple and reproducible [Kubernetes](https://kubernetes.io/) playground locally.

## Prerequisites
Provisioning a cluster requires that the following software installed on your system:
* [Vagrant](https://www.vagrantup.com/)
* [Ansible](https://www.ansible.com/)
* [VirtualBox](https://www.virtualbox.org/)

Once provisioned, the cluster can be accessed with [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).

For each service deployed, the local /etc/hosts file is updated with the appropriate ip->hostname mapping. As this requires local privilege escalation, run to following commands to allow ansible to run as sudo.

```bash
# Generate a random password file for the ansible vault
mkdir -p ~/.secrets
chmod u=rwx,g=,o= ~/.secrets
openssl rand -base64 32 > ~/.secrets/ansible-vault
# Provide ansible with the password for local priviledge escalatiuon
printf "---\nansible_become_pass: %s" "Your Password" > ansible/host_vars/localhost/become_pass.yml
# Encrypt the password file with ansible vault
ansible-vault encrypt --vault-password-file ~/.secrets/ansible-vault \
                      --encrypt-vault-id default \
                      ansible/host_vars/localhost/become_pass.yml
```

## Provisioning
To provision the cluster, run: `vagrant up`.

To destroying the cluster, run: `vagrant destroy -f`

## Configuration
The default cluster configuration can be changed by setting the following environment variables.

Variable      | Default                | Description
--------------|------------------------|------------------------------------------
OS            | alpine38               | Operating system (only tested with alpine)
BOX_REPO      | generic                | Vagrant repository
FIRST_IP      | 10.0.1.10              | First ip address in cluster (make sure the subnet does not overlap with the host machine)
DOMAIN        | project_folder.local   | Cluster domain
CLUSTER_GROUP | cluster                | Ansible group containing all hosts
PLAYBOOK      | ansible/k3s.yml        | Playbook file
ANSIBLE_CFG   | ansible/ansible.cfg    | Ansible configuration file
NUM_MASTER    | 1                      | Number of master nodes
NUM_WORKER    | 3                      | Number of worker nodes

## Docs
[Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Vagrant](https://www.vagrantup.com/docs)

[Kubernetes API](https://v1-17.docs.kubernetes.io/docs/reference/generated/kubernetes-api/v1.17/)
