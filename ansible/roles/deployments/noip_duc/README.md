# NoIP Dynamic Update Client
Deploy a pod that update your [NoIP](https://my.noip.com/) dynamic DNS hostnames periodically.

Set the variables `noip_username` and `noip_password` in vars/main.yml.
Optionally you can set the `noip_interval` variable to change how often the IP address is updated (default is 30 minutes)

Note that currently all hostnames on your account are updated if you do not have an [enhanced account](https://www.noip.com/support/knowledgebase/limit-hostnames-updated-dynamic-dns-client/).
As I do not have an enhanced account. this feature is untested. Please refer to the documentation if you encounter any issues with this feature.

## Example
```yaml
# vars/main.yml
noip_username: your_username
noip_password: your_password # or group password if applicable
noip_interval: 5 # [optional] update IP every 5 minutes
noip_group: my_cluster_name # [optional] noip group containing the hostnames
```

## Encryption
Remember to encrypt `vars/main.yml` with ansible-vault.

```bash
ansible-vault encrypt --vault-password-file=~/.secrets/ansible-vault vars/main.yml
```
