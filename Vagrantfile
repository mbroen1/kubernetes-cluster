require 'ipaddr'

PRJ_HOME  = File.dirname(__FILE__)
PRJ       = File.basename(PRJ_HOME)

OS              = (ENV['OS'] || "alpine38")
BOX_REPO        = (ENV['BOX_REPO'] || "generic")
FIRST_IP        = (ENV['FIRST_IP'] || "10.0.1.10")
FIRST_PUBLIC_IP = (ENV['FIRST_IP'] || "10.0.0.200")
BRIDGE_IFACE    = (ENV['BRIDGE_IFACE'] || "wlp3s0")
DOMAIN          = (ENV['DOMAIN'] || "#{PRJ}.local")
CLUSTER_GROUP   = (ENV['CLUSTER_GROUP'] || 'cluster')
PLAYBOOK        = (ENV['PLAYBOOK'] || 'ansible/k3s.yml')
ANSIBLE_CFG     = (ENV['ANSIBLE_CFG'] || 'ansible/ansible.cfg')

NODES = {
  master:  (ENV['NUM_MASTER']  || 1).to_i,
  worker:  (ENV['NUM_WORKER']  || 3).to_i,
}


Vagrant.configure("2") do |config|
  ip     = IPAddr.new FIRST_IP
  pub_ip = IPAddr.new FIRST_PUBLIC_IP
  config.vm.provider "virtualbox" do |v|
    v.linked_clone          = true
    v.check_guest_additions = false
    v.memory                = 1024
    v.cpus                  = 2
  end

  NODES.each_with_index do |(role, count), g_index|
    (1..count).each_with_index do |node_num, n_index|
      config.vm.define "#{PRJ}_#{role}-#{node_num}" do |node|
        provision(node.vm, role, node_num, ip.to_s, pub_ip.to_s)
        ip = ip.succ
        pub_ip = pub_ip.succ
        # Run ansible provisioning once the last node is created
        if g_index == NODES.size - 1 and n_index == count - 1
          ansible_provisioning(node.vm)
        end
      end
    end
  end

end

def provision(vm, node_role, node_num, ip_addr, public_ip_addr)
  vm.box      = (ENV["OS_#{node_role}_#{node_num}"] || "#{BOX_REPO}/#{OS}")
  vm.network    "private_network", ip: ip_addr
  set_name(vm, node_role, node_num)
  install_python(vm)
  case node_role
  when /master/
    vm.network "public_network", bridge: "#{BRIDGE_IFACE}", ip: public_ip_addr
    vm.network :forwarded_port, guest: 6443, host: 6443
  end
end

def set_name(vm, node_role, node_num)
  vm.hostname = "%s-%s-%03d.%s" % [PRJ, node_role, node_num, DOMAIN]
  vm.provider "virtualbox" do |v|
    v.name = "%s_%s-%d" % [PRJ, node_role, node_num]
  end
end

def install_python(vm)
  case OS
  when /alpine/
    vm.provision "shell",
      inline: "apk add --update --no-cache python3"
  end
end

def groups()
  return NODES.map { |k,v| [k, [ k, v ]] }
    .transpose
    .collect { |v| v.all?(Symbol) ? [[:"#{CLUSTER_GROUP}:children", v.map{|v| v.to_s}]]
                                  : v.map { |k,v| [k, (1..v).map { |n| "#{PRJ}_#{k}-#{n}" }] } }
    .flatten(1)
    .to_h
end

def ansible_provisioning(vm)
  vm.provision :ansible do |ansible|
    ansible.limit = "all,localhost"
    ansible.verbose = true
    ansible.playbook = "#{PLAYBOOK}"
    ansible.config_file = "#{ANSIBLE_CFG}"
    ansible.groups = groups()
    ansible.extra_vars = {
      domain: DOMAIN
    }
  end
end
